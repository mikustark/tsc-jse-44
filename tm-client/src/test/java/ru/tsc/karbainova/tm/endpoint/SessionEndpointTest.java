package ru.tsc.karbainova.tm.endpoint;

import lombok.NonNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.karbainova.tm.marker.SoapCategory;

public class SessionEndpointTest {
    @NonNull
    private static final SessionEndpointService sessionEndpointService = new SessionEndpointService();
    @NonNull
    private static final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @Nullable
    private static SessionDTO session;

    private static String userLogin = "admin";

    @BeforeClass
    public static void beforeClass() {
        session = sessionEndpoint.openSession(userLogin, userLogin);
    }

    @Test
    @Category(SoapCategory.class)
    public void open() {
        Assert.assertNotNull(session);
    }
}
