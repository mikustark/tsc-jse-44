package ru.tsc.karbainova.tm.endpoint;

import lombok.NoArgsConstructor;
import ru.tsc.karbainova.tm.api.service.ServiceLocator;

@NoArgsConstructor
public class AbstractEndpoint {
    protected ServiceLocator serviceLocator;

    public AbstractEndpoint(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }
}
