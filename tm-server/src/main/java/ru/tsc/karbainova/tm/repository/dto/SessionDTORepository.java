package ru.tsc.karbainova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.repository.dto.ISessionDTORepository;
import ru.tsc.karbainova.tm.dto.SessionDTO;

import javax.persistence.EntityManager;
import java.util.List;

public class SessionDTORepository extends AbstractDTORepository<SessionDTO> implements ISessionDTORepository {

    public SessionDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear() {
        entityManager
                .createQuery("DELETE FROM SessionDTO e")
                .executeUpdate();
    }

    @Override
    public List<SessionDTO> findAll() {
        return entityManager.createQuery("SELECT e FROM SessionDTO e", SessionDTO.class).getResultList();
    }

    @Override
    public List<SessionDTO> findAllByUserId(@NotNull final String userId) {
        return entityManager.createQuery("SELECT e FROM SessionDTO e WHERE e.userId = :userId", SessionDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public SessionDTO findById(@Nullable final String id) {
        return entityManager.find(SessionDTO.class, id);
    }

    @Override
    public void removeById(@Nullable final String id) {
        SessionDTO reference = entityManager.getReference(SessionDTO.class, id);
        entityManager.remove(reference);
    }

    @Override
    public void removeByUserId(@NotNull final String userId) {
        entityManager
                .createQuery("DELETE FROM SessionDTO e WHERE e.userId = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public int getCount() {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM SessionDTO e", Long.class)
                .getSingleResult()
                .intValue();
    }

}

