package ru.tsc.karbainova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.repository.model.IProjectRepository;
import ru.tsc.karbainova.tm.model.Project;

import javax.persistence.EntityManager;
import java.util.List;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear() {
        entityManager
                .createQuery("DELETE FROM ProjectDTO e")
                .executeUpdate();
    }

    @Override
    public void clearByUserId(@NotNull final String userId) {
        entityManager
                .createQuery("DELETE FROM ProjectDTO e WHERE e.userId = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    public @NotNull Project findById(@Nullable final String id) {
        return entityManager.find(Project.class, id);
    }

    @Override
    public Project findByName(@Nullable final String userId, @Nullable final String name) {
        return entityManager
                .createQuery(
                        "SELECT e FROM ProjectDTO e WHERE e.name = :name AND e.userId = :userId",
                        Project.class
                )
                .setParameter("name", name)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public List<Project> findAll() {
        return entityManager.createQuery("SELECT e FROM ProjectDTO e", Project.class)
                .getResultList();
    }

    @Override
    public List<Project> findAllByUserId(@Nullable final String userId) {
        return entityManager
                .createQuery("SELECT e FROM ProjectDTO e WHERE e.userId = :userId", Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public Project findByIdUserId(@Nullable final String userId, @Nullable final String id) {
        return entityManager
                .createQuery(
                        "SELECT e FROM ProjectDTO e WHERE e.id = :id AND e.userId = :userId", Project.class
                )
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public Project findByIndex(@Nullable final String userId, @NotNull final Integer index) {
        return entityManager
                .createQuery("SELECT e FROM ProjectDTO e WHERE e.userId = :userId", Project.class)
                .setParameter("userId", userId)
                .setFirstResult(index).setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void remove(@NotNull final Project entity) {
        Project reference = entityManager.getReference(Project.class, entity.getId());
        entityManager.remove(reference);
    }

    @Override
    public void removeById(@Nullable final String id) {
        Project reference = entityManager.getReference(Project.class, id);
        entityManager.remove(reference);
    }

    @Override
    public void removeByName(@Nullable final String userId, @Nullable final String name) {
        entityManager
                .createQuery("DELETE FROM ProjectDTO e WHERE e.name = :name AND e.userId = :userId")
                .setParameter("userId", userId)
                .setParameter("name", name)
                .executeUpdate();
    }

    @Override
    public void removeByIdAndUserId(@Nullable final String userId, @NotNull final String id) {
        entityManager
                .createQuery("DELETE FROM ProjectDTO e WHERE e.userId = :userId AND e.id=:id")
                .setParameter("id", id)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void removeByIndex(@NotNull final String userId, final int index) {
        entityManager
                .createQuery("DELETE FROM ProjectDTO e WHERE e.userId = :userId")
                .setParameter("userId", userId)
                .setFirstResult(index).setMaxResults(1).executeUpdate();
    }

    @Override
    public int getCount() {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM ProjectDTO e", Long.class)
                .getSingleResult()
                .intValue();
    }

    @Override
    public int getCountByUser(@NotNull String userId) {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM ProjectDTO e WHERE e.userId = :userId", Long.class)
                .setParameter("userId", userId)
                .getSingleResult()
                .intValue();
    }

}
