package ru.tsc.karbainova.tm.service.model;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.repository.model.ITaskRepository;
import ru.tsc.karbainova.tm.api.service.IConnectionService;
import ru.tsc.karbainova.tm.api.service.model.ITaskServiceModel;
import ru.tsc.karbainova.tm.exception.empty.EmptyIdException;
import ru.tsc.karbainova.tm.exception.empty.EmptyNameException;
import ru.tsc.karbainova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.karbainova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.karbainova.tm.model.Task;
import ru.tsc.karbainova.tm.repository.model.TaskRepository;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class TaskService extends AbstractOwnerService<Task> implements ITaskServiceModel {

    public TaskService(IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    @SneakyThrows
    public List<Task> findAll() {
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            return taskRepository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public List<Task> findAllTaskByUserId(String userId) {
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            return taskRepository.findAllByUserId(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(Collection<Task> collection) {
        if (collection == null) return;
        for (Task i : collection) {
            add(i);
        }
    }

    public java.sql.Date prepare(final Date date) {
        if (date == null) return null;
        return (java.sql.Date) new Date(date.getTime());
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NonNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public Task add(Task task) {
        if (task == null) throw new TaskNotFoundException();
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            @NonNull final ITaskRepository TaskRepository = new TaskRepository(entityManager);
            TaskRepository.add(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NonNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void create(@NonNull String userId, @NonNull String name, @NonNull String description) {
        if (name == null || name.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NonNull final ITaskRepository TaskRepository = new TaskRepository(entityManager);
            TaskRepository.add(task);
            entityManager.getTransaction().commit();
        } catch (@NonNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@NonNull String userId, @NonNull Task task) {
        if (task == null) throw new TaskNotFoundException();
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.removeByIdUserId(userId, task.getId());
            entityManager.getTransaction().commit();
        } catch (@NonNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public Task updateById(@NonNull String userId, @NonNull String id,
                           @NonNull String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final ITaskRepository taskRepository = new TaskRepository(entityManager);

            Task task = taskRepository.findByIdUserId(userId, id);
            if (task == null) throw new ProjectNotFoundException();
            task.setName(name);
            task.setDescription(description);
            final ITaskRepository repository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
            return task;
        } catch (@NonNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public Task findByName(@NonNull String userId, @NonNull String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            return taskRepository.findByName(userId, name);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public Task findByIdUserId(@NonNull String userId, @NonNull String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyNameException();
        @NonNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NonNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            return taskRepository.findByIdUserId(userId, id);
        } finally {
            entityManager.close();
        }
    }
}
