--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

-- Started on 2021-06-29 14:31:22

SET
statement_timeout = 0;
SET
lock_timeout = 0;
SET
idle_in_transaction_session_timeout = 0;
SET
client_encoding = 'UTF8';
SET
standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET
check_function_bodies = false;
SET
xmloption = content;
SET
client_min_messages = warning;
SET
row_security = off;

SET
default_tablespace = '';

SET
default_table_access_method = heap;

--
-- TOC entry 200 (class 1259 OID 16398)
-- Name: tm_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tm_user
(
    id            character varying(250) NOT NULL,
    login         character varying(250) NOT NULL,
    password_hash character varying(250),
    email         character varying(250),
    first_name    character varying(250),
    last_name     character varying(250),
    middle_name   character varying(250),
    role          character varying(250),
    locked        boolean
);


ALTER TABLE public.tm_user OWNER TO postgres;

--
-- TOC entry 2992 (class 0 OID 16398)
-- Dependencies: 200
-- Data for Name: tm_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tm_user (id, login, password_hash, email, first_name, last_name, middle_name, role, locked) FROM stdin;
eb30e5b5
-4538-4476-adcd-edff551c5fb4	test	a164336b34d5db17074cfb28a2a5d52f	\N	\N	\N	\N	USER	f
559da71b-bc87-4620-836d-6e1a3d9336a4	admin	e2858f2e75ae1a633c5941d27edcfec2	\N	\N	\N	\N	USER	f
\.


--
-- TOC entry 2861 (class 2606 OID 16405)
-- Name: tm_user user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tm_user
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


-- Completed on 2021-06-29 14:31:22

--
-- PostgreSQL database dump complete
--

